/* Mepi
    - Block

*/

#include "stdafx.h"
#include "Block.h"

Block::Block()
    : _blockType(BlockType::none), _isAlive(true)
{
}

Block::~Block() 
{    
}

void Block::RemoveType()
{
    _blockType = BlockType::none;
    _isAlive = false;
}