/* Mepi
    - Block Container

*/

#include "stdafx.h"
#include "BlockContainer.h"

BlockContainer::BlockContainer(int16_t height, int16_t width)
    : _height(height), _width(width)
{
    _blocks = new Block*[_height];
    for (int16_t h = 0; h < _height; h++)
    {
        _blocks[h] = new Block[_width];
    }
}

BlockContainer::~BlockContainer()
{
    safe_delete_array(_blocks);
}

BlockContainer::ELocationStatus BlockContainer::isValidateLocation(SBlockLocation _val1, SBlockLocation _val2) const
{
    if ((_val1.height <= 0 || _height < _val1.height))
        return ELocationStatus::wrong_height;    
    
    if ((_val1.width <= 0 || _width <= _val1.width - 1))
        return ELocationStatus::wrong_width;

    if ((_val1.height == _val2.height) && (_val1.width == _val2.width))
        return ELocationStatus::same_location;    
    
    return ELocationStatus::ok;
}

BlockContainer::ELocationStatus BlockContainer::SwapValidity(SBlockLocation first, SBlockLocation second)
{
    // TODO : 좌표 유효성 체크( 보강할 점이 있는지 확인)
    // TODO : 교환 가능한 근접 블럭인지 체크
    auto validType = isValidateLocation(first, second);
    if (ELocationStatus::ok != validType)
    {
        std::cout << "#Error# : wrong location Index!" << std::endl;
        return validType;
    }
    
    // 유효성 통과 교체.
    // PSEUDO : first tmp에 담아두고
    auto tmpType = GET_BLOCK_TYPE(first.height, first.width);

    // first의 타입을 second타입으로 덮어 쓰고
    GET_BLOCK(first.height, first.width).setType(GET_BLOCK_TYPE(second.height, second.width));
        
    // seconde 타입을 tmp타입으로 덮어 쓴다.
    GET_BLOCK(second.height, second.width).setType(tmpType);

    return ELocationStatus::ok;
}

void BlockContainer::RemoveBlock(SBlockLocation* blockLocs, int16_t cnt /*TODO 임시변수*/)
{
    for (int i = 0; i < cnt; i++)
    {
        GET_BLOCK(blockLocs[i].height, blockLocs[i].width).RemoveType();
    }
}
