/* Mepi
    - Match Game

*/

#include "stdafx.h"
#include "MatchGame.h"
#include <iomanip>

using BLOCK_TYPE = Block::BlockType;

MatchGame::MatchGame(int16_t height, int16_t width)
{
    CreateField(height, width);
    CreateRandomBlock();
    UpdateField();
}

MatchGame::~MatchGame()
{
    DestroyField();
}

void MatchGame::CreateField(int16_t height, int16_t width)
{
    if (nullptr == _field)
        _field = new Field(height, width);
}

void MatchGame::DestroyField()
{
    safe_delete(_field);
}

void MatchGame::CreateRandomBlock()
{
    BlockContainer* blockContainer = getBlockContainer();
    if (nullptr == blockContainer)
        return;

    int16_t height = blockContainer->getMaxHeight();
    int16_t width = blockContainer->getMaxWidth();

    for (int16_t h = height - 1; 0 <= h; --h)
    {
        for (int16_t w = 0; w < width; w++)
        {
            if (w == 0 || h == 0 || w == width - 1)
                blockContainer->setType(h, w, BLOCK_TYPE::wall);
            else
                blockContainer->setType(h, w, static_cast<BLOCK_TYPE>(rand() % 4 + 2));
        }
    }
}

void MatchGame::RePositionBlocks()
{
    // TODO : 빈자리가 있다면 내리고 채워 넣자
    // PSEUDO : 자신의 아래칸이 비어 있다면 아래로 한칸 이동
        // 자신의 아래칸이 비어있지 않을때까지


    //  TODO : 중간에 비어있는칸이 모두 채워지면 빈자리에 새로운 블럭 생성 
}


void MatchGame::SelectLocation(SBlockLocation& blockLocation)
{
    int height, width;
    std::cin >> height >> width;

    blockLocation.height = height;
    blockLocation.width = width;

    // TODO : Test Code
    std::cout << getBlockContainer()->getType(height, width) << std::endl;

    // TODO : 일단 간단하게 문자열 파싱 방식은 나중에 생각해보자.
    /*
    char buff[255];
    std::cin.getline(buff, 255);

    int firstIdx[2];
    int* pIdx = firstIdx;
    for (int i = 0; i < 3; i++)
    {
    if (!isspace(static_cast<int>(buff[i])) && '\0' != *buff)
    {
    *pIdx = static_cast<int>(buff[i] - '0');
    std::cout << *pIdx << std::endl;
    pIdx++;
    }
    }
    */
}

void MatchGame::UpdateField()
{
    BlockContainer* blockContainer = getBlockContainer();
    if (nullptr == blockContainer)
        return;

    int16_t height = blockContainer->getMaxHeight();
    int16_t width = blockContainer->getMaxWidth();

    std::cout << "######## 3-Match Puzzle Game ########\n\n";
    for (int16_t h = height - 1; 0 <= h; --h)
    {
        std::cout << h << "\t";
        for (int16_t w = 0; w < width; w++)
        {
            std::cout << blockContainer->getType(h, w) << " ";
        }
        std::cout << std::endl;
    }
        
    std::cout << std::setw(10);
    for (int16_t w = 0; w < width; w++)
    {
        std::cout << w << "  ";
    }
    std::cout << "\n───────────────────" << std::endl;
    std::cout << "H" << std::endl;
    std::cout << "/ W \t" << "< input location (Height Width) > : Ex> First : 1 2" << std::endl;
}


bool MatchGame::Swap(SBlockLocation first, SBlockLocation second)
{
    auto status = getBlockContainer()->SwapValidity(first, second);
    
    if (BlockContainer::ELocationStatus::ok != status)
    {
        return false;
    }

    return true;

    // TODO : Test Code -----------------------------------------/
    /*
    SBlockLocation blocks[2];
    blocks[0] = first;
    blocks[1] = second;
    getBlockContainer()->RemoveBlock(blocks, sizeof(blocks) / sizeof(*blocks));
    */
    // ----------------------------------------------------------/
}

void MatchGame::CheckBlockCrashValidate()
{
    // 삭제 가능한 블럭이 있는지 체크.
    //
}

