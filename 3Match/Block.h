#pragma once

#include <iostream>

class Block
{
public:
    enum class BlockType
    {
        none = 0,
        wall,   // ��
        red,    // ��
        blue,   // ��
        yellow, // ��
        green,  // ��
        white,  // ��
    };

public:
    Block();
    ~Block();

public:
    BlockType getType() const { return _blockType; }

    void setType(BlockType type) { _blockType = type; }

    void RemoveType();
    
private:
    BlockType _blockType;
    bool _isAlive;

};

//std::ostream& operator << (std::ostream& os, const Block::BlockType& b);
inline std::ostream& operator << (std::ostream& os, const Block::BlockType& b)
{
    auto type = static_cast<std::underlying_type<Block::BlockType>::type>(b);
    switch (type)
    {
        case 0: return os << "  ";
        case 1: return os << "��";
        case 2: return os << "��";
        case 3: return os << "��";
        case 4: return os << "��";
        case 5: return os << "��";
        case 6: return os << "��";
    }

    return os;
}


