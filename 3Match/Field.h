#pragma once

#include <cstdint>

class BlockContainer;

class Field
{    
public:
    Field(int16_t height, int16_t width);
    ~Field() ;
    
public:
    BlockContainer* getBlockContainer() const { return _blockContainer; }

private:
    BlockContainer* _blockContainer;
};
