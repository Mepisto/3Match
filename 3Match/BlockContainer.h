#pragma once

#include <cstdint>
#include "Block.h"
#include "GameUtil.h"

using BLOCK_TYPE = Block::BlockType;

struct SBlockLocation
{
    int height = 0;
    int width = 0;
    SBlockLocation(int h = 0, int w = 0) : height(h), width(w) {}
};
//template<typename T, int N> inline auto begin(T(&r)[N]) { return r; }
//template<typename T, int N> inline auto end(T(&r)[N]) { return r+N; }
//template<int N> inline auto begin(T(&r)[N]) { return &(p); }
//inline auto end(SBlockLocation& p, int length) { return &(p) + length; }

#define GET_BLOCK(H, W) _blocks[H][W]
#define GET_BLOCK_TYPE(H, W) GET_BLOCK(H, W).getType()

class BlockContainer
{
    friend class MatchGame;

public:
    enum class ELocationStatus
    {
        ok = 0,
        wrong_height,
        wrong_width,
        same_location
    };

public:
    BlockContainer() = delete;

    explicit BlockContainer(int16_t height, int16_t width);
    ~BlockContainer();

private:
    const int16_t getMaxHeight() const { return _height; }
    const int16_t getMaxWidth() const { return _width; }
    BLOCK_TYPE getType(int16_t height, int16_t width) const { return GET_BLOCK_TYPE(height, width); }
    void setType(int16_t height, int16_t width, BLOCK_TYPE type) { GET_BLOCK(height, width).setType(type); }

private:
    // 좌표 유효성 체크.
    ELocationStatus isValidateLocation(SBlockLocation _val1, SBlockLocation _val2) const;
    // 선택된 블럭 2개 자리 교체.
    ELocationStatus SwapValidity(SBlockLocation first, SBlockLocation second);
    // 매칭된 블럭 삭제.
    void RemoveBlock(SBlockLocation* blocks, int16_t cnt /*TODO 임시변수*/);    

private:
    int16_t _height, _width;
    Block** _blocks;
};

