#pragma once

#include "Field.h"
#include "BlockContainer.h"
#include "BlockChecker.h"

class MatchGame
{
public:
    MatchGame() = delete;
    MatchGame(const MatchGame&) = delete;
    MatchGame& operator = (const MatchGame&) = delete;

    MatchGame(int16_t height, int16_t width);
    ~MatchGame();

public:
    BlockContainer* getBlockContainer() const { return _field->getBlockContainer(); }
            
private:    

    // 필드 생성.
    void CreateField(int16_t height, int16_t width);
    // 필드 파괴.
    void DestroyField();
    // 초기 최초 랜덤하게 블럭을 생성한다.
    void CreateRandomBlock();
    // 삭제된 블럭이 있다면 재정렬 한다.
    void RePositionBlocks();

public:
    // Main Update    
    void UpdateField();
    // 사용자 입력에 따라 블럭을 선택한다.
    void SelectLocation(SBlockLocation& blockLocation);
    // 블럭 위치를 바꿔준다
    bool Swap(SBlockLocation first, SBlockLocation second);
    // 3Match 조건에 맞는 블럭이 있는지 체크.
    void CheckBlockCrashValidate();
    

private:
    Field* _field = nullptr;
    BlockChecker* _blockChecker = nullptr;
};

