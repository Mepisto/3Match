/* Mepi
    - 게임 유틸함수 정의.
*/


#pragma once


template <typename T>
inline void safe_delete(T*& p)
{
    delete p;
    p = static_cast<T *>(0);
};

template <typename T>
inline void safe_delete_array(T*& p)
{
    delete[] p;
    p = static_cast<T *>(0);
}