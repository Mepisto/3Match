/* Mepi
    - Field

*/

#include "stdafx.h"
#include "Field.h"
#include "BlockContainer.h"

Field::Field(int16_t height, int16_t width)
{
    _blockContainer = new BlockContainer(height, width);
}

Field::~Field()
{
    safe_delete(_blockContainer);
}
