// 3Match.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//
/* Mepi
    Main

*/

#include "stdafx.h"
#include <ctime>
#include <windows.h>

#include "MatchGame.h"

MatchGame* MGame;

// 콘솔 커서 포지션 조절 함수 아직 사용 하지 않음.
void gotoxy(int16_t x, int16_t y)
{
    COORD p = { x, y };
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), p);
}

// wall padding + 1 수치
const int16_t fieldHeight = 13;
// wall padding + 1 수치
const int16_t fieldWidth = 9;

int main()
{
    std::srand((unsigned int)time(NULL));

    MGame = new MatchGame(13, 9);
    
    bool bPlayAgain = true;
    do
    {
        // TODO : 클리어 조건을 충족하거나 제한에 걸리면 false 처리 하며 끝내야함.
        //       ecs 키 누를때 강제 종료.
        /*if (GetAsyncKeyState(VK_ESCAPE))        
        {
            bPlayAgain = false;
            std::cin.ignore();
        }
        else
            bPlayAgain = true;*/        
        
        // TODO : Input 따로 빼자. --------------/
        //  방향키를 움직여서 블럭을 선택(Enter and Space) 하도록... gotoxy(11, 11);
        SBlockLocation first, second;                
        std::cout << "First : ";
        MGame->SelectLocation(first);
        std::cout << "Second : ";
        MGame->SelectLocation(second);
        // -------------------------------------/
        
        // 선택된 블럭 유효성 체크 후 위치 교환. 
        if (!MGame->Swap(first, second))
        {
            // 유효성 체크 에러.
            continue;
        }

        // TODO : 3Match 체크 로직. 3Match에 성공한 블럭들 찾아서 삭제 후 재배치.
        MGame->CheckBlockCrashValidate();
        
        // TODO : 삭제 할 것 테스트용.------------------/
        /*std::cout << "Continue (y/n) : ";
        char buff[255];
        std::cin >> buff;
        bPlayAgain = (*buff == 'y' || *buff == 'Y');*/
        // -------------------------------------------/
                
        system("cls");

        MGame->UpdateField();

    } while (bPlayAgain);


    safe_delete(MGame);    
    
    return 0;
}
