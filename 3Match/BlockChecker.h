#pragma once

#include <cstdint>
class Block;
struct SBlockLocation;

class BlockChecker
{
public:
    BlockChecker();
    ~BlockChecker();

public:
    // TODO : 매칭된 타겟을 어떤 자료구조로 넘겨 줄지 고민 필요.
    void CheckValidate(const Block& targetBlock);
    // 행 체크 (+)우측, (-)좌측
    void CheckRow(int direction);
    // 열 체크 (+)위, (-)아래
    void CheckColumn(int direction);

private:
    int16_t _fieldHeight;
    int16_t _fieldWidth;
    const SBlockLocation* _curTargetLoc;
};